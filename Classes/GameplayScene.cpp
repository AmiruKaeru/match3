/************************************************************************
    match3
    Copyright (C) 2020  Amiyo Hisamov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "GameplayScene.h"

#include "ui/CocosGUI.h"
#include <sstream>
#include <algorithm>

#include "MainMenu.h"

USING_NS_CC;

static GridSize gridSize = GridSize::OCTO;

Scene* GameplayScene::createScene(const GridSize &size)
{
    gridSize = size;
    return GameplayScene::create();
}

bool GameplayScene::init()
{
    if(!Scene::init())
    {
        return false;
    }

    this->addChild(createBackground());

    _itemTexture =
    {
        { ItemType::APPLE, Director::getInstance()->getTextureCache()->addImage("Fruits/Apple.png") },
        { ItemType::BANANAS, Director::getInstance()->getTextureCache()->addImage("Fruits/Bananas.png") },
        { ItemType::CHERRIES, Director::getInstance()->getTextureCache()->addImage("Fruits/Cherries.png") },
        { ItemType::KIWI, Director::getInstance()->getTextureCache()->addImage("Fruits/Kiwi.png") },
        { ItemType::MELON, Director::getInstance()->getTextureCache()->addImage("Fruits/Melon.png") },
        { ItemType::ORANGE, Director::getInstance()->getTextureCache()->addImage("Fruits/Orange.png") },
        { ItemType::PINEAPPLE, Director::getInstance()->getTextureCache()->addImage("Fruits/Pineapple.png") },
        { ItemType::STRAWBERRY, Director::getInstance()->getTextureCache()->addImage("Fruits/Strawberry.png") }
    };
    createItemAnimation();

    auto mouseListener = EventListenerMouse::create();
    mouseListener->onMouseDown = CC_CALLBACK_1(GameplayScene::onMouseDown, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(mouseListener, this);

    auto size = static_cast<int>(gridSize);

    this->addChild(createScore());

    _gridBG = Director::getInstance()->getTextureCache()->addImage("Background/Brown.png");
    _gridNode = Node::create();
    _gridNode->setContentSize(_gridBG->getContentSize() * size);
    createGrid();
    this->addChild(_gridNode);

    this->addChild(createExitButton());

    return true;
}

Sprite* GameplayScene::createBackground()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();

    auto bg = Sprite::create("Background/Green.png", Rect(0, 0, visibleSize.width, visibleSize.height));
    bg->getTexture()->setTexParameters(
    {
        backend::SamplerFilter::NEAREST, backend::SamplerFilter::NEAREST,
        backend::SamplerAddressMode::REPEAT, backend::SamplerAddressMode::REPEAT
    });
    bg->setPosition(visibleSize.width / 2, visibleSize.height / 2);

    return bg;
}

Label* GameplayScene::createScore()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();

    _score = Label::createWithTTF("0", "fonts/arial.ttf", 32);
    _score->enableOutline(Color4B::BLACK, 2);
    _score->setPosition(visibleSize.width / 2, visibleSize.height * 0.9);

    return _score;
}

void GameplayScene::createGrid()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    auto size = static_cast<int>(gridSize);
    auto bgSize = _gridBG->getContentSize();

    _gridNode->removeAllChildrenWithCleanup(false);

    _gridNode->setPosition((visibleSize.width / 2) - ((bgSize.width * size) / 2),
                           (visibleSize.height / 2) - ((bgSize.height * size) / 2));

    auto frameTexture = Director::getInstance()->getTextureCache()->addImage("Transition.png");

    for(auto y = 0; y < size; ++y)
    {
        for(auto x = 0; x < size; ++x)
        {
            auto sprite = Sprite::createWithTexture(_gridBG);
            sprite->setAnchorPoint(Vec2::ZERO);
            sprite->setPosition(bgSize.width * x, bgSize.height * y);

            auto center = bgSize / 2;

            auto frame = Sprite::createWithTexture(frameTexture);
            frame->setPosition(center);
            frame->setScale(bgSize.width / frameTexture->getContentSize().width);
            sprite->addChild(frame);
            sprite->addChild(createItem());
            sprite->setName(std::to_string(x) + "," + std::to_string(y));

            _gridNode->addChild(sprite);
        }
    }

    _gridNode->scheduleOnce([this](float) // after createAnim
    {
        checkGrid();
    }, 0.5f, "checkGrid");
    lockInput(1.0f);
}

Sprite* GameplayScene::createItem(const ItemType &type)
{
    auto texture = _itemTexture[type];
    auto sprite = Sprite::createWithTexture(texture);
    auto frameSize = Size(texture->getContentSize().height, texture->getContentSize().height);
    sprite->setTextureRect(Rect(Vec2::ZERO, frameSize));
    sprite->setPosition(_gridBG->getContentSize() / 2);
    sprite->setScale(_gridBG->getContentSize().height / texture->getContentSize().height);
    sprite->setName(ItemTypeToString(type));

    Vector<SpriteFrame*> frames;
    frames.reserve((texture->getContentSize().width / texture->getContentSize().height) - 1);

    for(auto i = 1; i <= frames.capacity(); ++i)
    {
        auto rect = Rect(Vec2(texture->getContentSize().height * i, 0), frameSize);
        frames.pushBack(SpriteFrame::createWithTexture(texture, rect));
    }

    auto animation = Animation::createWithSpriteFrames(frames, 0.05f);

    auto seq = Sequence::create({Animate::create(_itemAnim), Repeat::create(Animate::create(animation), -1)});

    sprite->runAction(seq);

    return sprite;
}

void GameplayScene::createItemAnimation()
{
    auto spritesheet = Director::getInstance()->getTextureCache()->addImage("Fruits/Collected.png");
    auto spriteSize = Vec2(spritesheet->getContentSize().height, spritesheet->getContentSize().height);

    Vector<SpriteFrame*> frames;
    frames.reserve(spritesheet->getContentSize().width / spriteSize.x);

    for(auto i = 0; i < frames.capacity(); ++i)
    {
        frames.pushBack(SpriteFrame::createWithTexture(spritesheet, Rect(Vec2(spriteSize.x * i, 0), Size(spriteSize))));
    }

    _itemAnim = Animation::createWithSpriteFrames(frames, 0.05f);
}

cocos2d::Node* GameplayScene::createExitButton()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    auto button = ui::Button::create("Close.png");
    button->setScale(5.0f);
    button->setAnchorPoint(Vec2(0, 1));
    button->setPosition(Vec2(0, visibleSize.height));
    button->getRendererNormal()->getTexture()->setTexParameters(
    {
        backend::SamplerFilter::NEAREST, backend::SamplerFilter::NEAREST,
        backend::SamplerAddressMode::CLAMP_TO_EDGE, backend::SamplerAddressMode::CLAMP_TO_EDGE
    });
    button->setName("ExitButton");
    button->addTouchEventListener([this](Ref* sender, ui::Widget::TouchEventType type)
    {
        if(type == ui::Widget::TouchEventType::ENDED)
        {
            gameOver();
        }
    });
    return button;
}

Node* GameplayScene::createGameOverMenu()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();

    TTFConfig labelConfig("fonts/arial.ttf", 52);
    labelConfig.outlineSize = 3;
    auto string = "Game Over!\nYour Score\n" + _score->getString() + "\nTry Again?";
    auto label = Label::createWithTTF(labelConfig, string, TextHAlignment::CENTER);

    Texture2D::TexParams tp =
    {
        backend::SamplerFilter::NEAREST, backend::SamplerFilter::NEAREST,
        backend::SamplerAddressMode::CLAMP_TO_EDGE, backend::SamplerAddressMode::CLAMP_TO_EDGE
    };
    auto playSprite = Sprite::create("Play.png");
    playSprite->getTexture()->setTexParameters(tp);
    auto play = MenuItemSprite::create(playSprite, nullptr,
                                       CC_CALLBACK_0(GameplayScene::restart, this));
    play->setScale(5.0f);

    auto closeSprite = Sprite::create("Close.png");
    closeSprite->getTexture()->setTexParameters(tp);
    auto close = MenuItemSprite::create(closeSprite, nullptr,
                                        CC_CALLBACK_0(Director::end, Director::getInstance()));
    close->setScale(5.0f);

    auto mainMenuSprite = Sprite::create("Levels.png");
    mainMenuSprite->getTexture()->setTexParameters(tp);
    auto mainMenu = MenuItemSprite::create(mainMenuSprite, nullptr,
                                           CC_CALLBACK_0(GameplayScene::mainMenu, this));
    mainMenu->setScale(3.5f);

    auto menu = Menu::createWithArray(
    {
        mainMenu,
        play,
        close
    });
    menu->alignItemsHorizontallyWithPadding(100);

    auto bg = Sprite::create();
    bg->setTextureRect(Rect(Vec2::ZERO, visibleSize / 2));
    bg->setColor(Color3B::BLACK);
    bg->setOpacity(255 * 0.75);
    bg->setPosition(visibleSize.width / 2, visibleSize.height / 2);
    bg->addChild(label);
    bg->addChild(menu);
    label->setAnchorPoint(Vec2(0.5, 1));
    label->setPosition(bg->getContentSize().width / 2, bg->getContentSize().height);
    menu->setAnchorPoint(Vec2(0.5, 0));
    menu->setPosition(bg->getContentSize().width / 2, bg->getContentSize().height * 0.2f);
    bg->setName("GameOver");

    return bg;
}

void GameplayScene::restart()
{
    Director::getInstance()->replaceScene(GameplayScene::createScene(gridSize));
}

void GameplayScene::addScore(int score)
{
    _score->setString(std::to_string(std::stoi(_score->getString()) + score));
}

void GameplayScene::onMouseDown(cocos2d::Event* event)
{
    if(!_isBusy)
    {
        auto mouseEvent = static_cast<EventMouse*>(event);
        auto mouseLocation = Director::getInstance()->convertToGL(mouseEvent->getLocation());
        if (_gridNode->getBoundingBox().containsPoint(mouseLocation))
        {
            auto point = _gridNode->convertToNodeSpace(mouseLocation)
                         / (_gridNode->getBoundingBox().size.height / static_cast<int>(gridSize));
            auto x = static_cast<int>(point.x);
            auto y = static_cast<int>(point.y);
            auto name = std::to_string(x) + "," + std::to_string(y);

            if (!_selectedItem)
            {
                _neighborhood =
                {
                    {std::to_string(x + 1) + "," + std::to_string(y)},
                    {std::to_string(x - 1) + "," + std::to_string(y)},
                    {std::to_string(x) + "," + std::to_string(y + 1)},
                    {std::to_string(x) + "," + std::to_string(y - 1)}
                };
                for(auto child : _gridNode->getChildren())
                {
                    if(child->getName() != name)
                    {
                        auto rate = _neighborhood.count(child->getName()) == 0 ? 0.5f : 0.75f;
                        child->getChildren().at(1)->setOpacity(255 * rate);
                    }
                    else
                    {
                        _selectedItem = child;
                    }
                }
            }
            else
            {
                auto neighbor = _gridNode->getChildByName(name);
                if(_neighborhood.count(name) > 0 && neighbor)
                {
                    lockInput(1.5f);
                    swapItems(neighbor, _selectedItem);

                    _gridNode->scheduleOnce([this, neighbor = _gridNode->getChildByName(neighbor->getName())](float)
                    {
                        if(!matchItem(neighbor))
                        {
                            matchItem(_selectedItem);
                        }
                        if(_matches.size() > 0)
                        {
                            moveItems();
                        }
                        else
                        {
                            swapItems(neighbor, _selectedItem);
                        }

                        _neighborhood.clear();
                        _selectedItem = nullptr;
                    }, 0.7f, "matchItems");

                    for(auto child : _gridNode->getChildren())
                    {
                        child->getChildren().at(1)->setOpacity(255);
                        child->setColor(Color3B::WHITE);
                    }
                }
            }
        }
    }
}

bool GameplayScene::matchItem(cocos2d::Node* node)
{
    auto position = StringToVec2(node->getName());
    auto type = node->getChildren().back()->getName();

    std::vector<std::pair<std::string, std::string>> variations =
    {
        std::make_pair(Vec2ToString(Vec2(position.x, position.y - 2)), Vec2ToString(Vec2(position.x, position.y - 1))),
        std::make_pair(Vec2ToString(Vec2(position.x, position.y - 1)), Vec2ToString(Vec2(position.x, position.y + 1))),
        std::make_pair(Vec2ToString(Vec2(position.x, position.y + 1)), Vec2ToString(Vec2(position.x, position.y + 2))),
        std::make_pair(Vec2ToString(Vec2(position.x - 2, position.y)), Vec2ToString(Vec2(position.x - 1, position.y))),
        std::make_pair(Vec2ToString(Vec2(position.x - 1, position.y)), Vec2ToString(Vec2(position.x + 1, position.y))),
        std::make_pair(Vec2ToString(Vec2(position.x + 1, position.y)), Vec2ToString(Vec2(position.x + 2, position.y))),
    };

    for(auto variant : variations)
    {
        auto first = _gridNode->getChildByName(variant.first);
        auto second = _gridNode->getChildByName(variant.second);
        if(first && second)
        {
            auto firstSprite = static_cast<Sprite*>(first->getChildByName(type));
            auto secondSprite = static_cast<Sprite*>(second->getChildByName(type));
            if(firstSprite && secondSprite)
            {
                _matches.insert(node);
                _matches.insert(first);
                _matches.insert(second);
                return true;
            }
        }
    }

    return false;
}

void GameplayScene::swapItems(Node *a, Node *b)
{
    auto aName = a->getName();
    a->setName(b->getName());
    b->setName(aName);

    auto moveToA = MoveTo::create(0.5f, a->getPosition());
    auto moveToB = MoveTo::create(0.5f, b->getPosition());

    a->runAction(moveToB);
    b->runAction(moveToA);
}

void GameplayScene::moveItems()
{
    for(auto node : _matches)
    {
        auto sprite = static_cast<Sprite*>(node->getChildren().at(1));
        sprite->stopAllActions();
        sprite->setName(ItemTypeToString(ItemType::NONE));
        sprite->runAction(Sequence::create(Animate::create(_itemAnim), FadeOut::create(0.1f), nullptr));

        auto positionInGrid = StringToVec2(node->getName());
        for(auto i = 1; i < static_cast<int>(gridSize) - positionInGrid.y; ++i)
        {
            auto name = Vec2ToString(Vec2(positionInGrid.x, positionInGrid.y + i));
            auto child = _gridNode->getChildByName(name);

            child->setName(node->getName());
            node->setName(name);

            auto t = child->getPosition();
            child->setPosition(node->getPosition());
            node->setPosition(t);
        }
    }

    for(auto node : _matches)
    {
        _gridNode->scheduleOnce([this, name = node->getName()](float)
        {
            auto node = _gridNode->getChildByName(name);
            node->removeChildByName(ItemTypeToString(ItemType::NONE));
            node->addChild(createItem());
        }, 0.7f, node->getName());
    }
    addScore(_matches.size());
    _matches.clear();
    _gridNode->scheduleOnce([this](float)
    {
        checkGrid();
    }, 1.5f, "checkGrid");
}

std::string GameplayScene::Vec2ToString(const Vec2 &vec)
{
    return std::to_string(static_cast<int>(vec.x)) + "," + std::to_string(static_cast<int>(vec.y));
}

Vec2 GameplayScene::StringToVec2(const std::string &string)
{
    std::stringstream stringstream(string);
    std::string token;
    std::vector<std::string> vector;
    while(std::getline(stringstream, token, ','))
    {
        vector.push_back(token);
    }
    return Vec2(std::stoi(vector[0]), std::stoi(vector[1]));
}

std::string GameplayScene::ItemTypeToString(const ItemType &type)
{
    switch(type)
    {
    case ItemType::APPLE:
        return "APPLE";
    case ItemType::BANANAS:
        return "BANANAS";
    case ItemType::CHERRIES:
        return "CHERRIES";
    case ItemType::KIWI:
        return "KIWI";
    case ItemType::MELON:
        return "MELON";
    case ItemType::ORANGE:
        return "ORANGE";
    case ItemType::PINEAPPLE:
        return "PINEAPPLE";
    case ItemType::STRAWBERRY:
        return "STRAWBERRY";
    }
    return "NONE";
}

GameplayScene::ItemType GameplayScene::StringToItemType(const std::string &string)
{
    if(string == "APPLE")
    {
        return ItemType::APPLE;
    }
    else if(string == "BANANAS")
    {
        return ItemType::BANANAS;
    }
    else if(string == "CHERRIES")
    {
        return ItemType::CHERRIES;
    }
    else if(string == "KIWI")
    {
        return ItemType::KIWI;
    }
    else if(string == "MELON")
    {
        return ItemType::MELON;
    }
    else if(string == "ORANGE")
    {
        return ItemType::ORANGE;
    }
    else if(string == "PINEAPPLE")
    {
        return ItemType::PINEAPPLE;
    }
    else if(string == "STRAWBERRY")
    {
        return ItemType::STRAWBERRY;
    }
    return ItemType::NONE;
}

GameplayScene::ItemType GameplayScene::Vec2ToItemType(const Vec2 &vec)
{
    auto node = _gridNode->getChildByName(Vec2ToString(vec));
    if(node)
    {
        return StringToItemType(node->getChildren().back()->getName());
    }
    return ItemType::NONE;
}

void GameplayScene::checkGrid()
{
    auto size = static_cast<int>(gridSize);
    for(auto y = 0; y < size; ++y)
    {
        for(auto x = 0; x < size; ++x)
        {
            auto name = Vec2ToString(Vec2(x, y));
            auto node = _gridNode->getChildByName(name);
            matchItem(node);
        }
    }
    if(_matches.size() > 0)
    {
        moveItems();
    }
    else if(!checkMovements())
    {
        gameOver();
    }
}

void GameplayScene::lockInput(float time)
{
    _gridNode->scheduleOnce([this](float)
    {
        _isBusy = false;
    }, time, "lockInput");
    _isBusy = true;
}

void GameplayScene::gameOver()
{
    _gridNode->pause();
    _score->setVisible(false);
    this->getChildByName("ExitButton")->setVisible(false);
    this->addChild(createGameOverMenu());
}

bool GameplayScene::checkMovements()
{
    auto size = static_cast<int>(gridSize);
    for(auto y = 0; y < size; ++y)
    {
        for(auto x = 0; x < size; ++x)
        {
            auto node = _gridNode->getChildByName(Vec2ToString(Vec2(x, y)));
            auto type = node->getChildren().back()->getName();

            if(checkNearItems(Vec2(x, y), StringToItemType(type)))
            {
                return true;
            }
        }
    }
    return false;
}

bool GameplayScene::checkNearItems(const Vec2 &pos, const ItemType &type)
{
    auto k = 1;
    auto isX = true;
    for(auto i = 0; i < 4; ++i)
    {
        isX = i < 2;
        auto pos1 = isX ? Vec2(pos.x + (2 * k), pos.y) : Vec2(pos.x, pos.y + (2 * k));
        auto pos2 = isX ? Vec2(pos.x + (3 * k), pos.y) : Vec2(pos.x, pos.y + (3 * k));
        if(Vec2ToItemType(pos1) == type && Vec2ToItemType(pos2) == type)
        {
            return true;
        }
        k *= -1;
    }
    return false;
}

void GameplayScene::mainMenu()
{
    Director::getInstance()->replaceScene(MainMenu::createScene());
}
