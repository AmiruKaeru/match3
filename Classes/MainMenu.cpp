/************************************************************************
    match3
    Copyright (C) 2020  Amiyo Hisamov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "MainMenu.h"

USING_NS_CC;

cocos2d::Scene* MainMenu::createScene()
{
    return MainMenu::create();
}

bool MainMenu::init()
{
    if ( !Scene::init() )
    {
        return false;
    }

    this->addChild(createBackground());

    this->addChild(createMenu());

    auto levelMenu = createLevelMenu();
    levelMenu->setVisible(false);
    this->addChild(levelMenu);

    return true;
}

cocos2d::Sprite* MainMenu::createBackground()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();

    auto bg = Sprite::create("Background/Green.png", Rect(0, 0, visibleSize.width, visibleSize.height));
    bg->getTexture()->setTexParameters(
    {
        backend::SamplerFilter::NEAREST, backend::SamplerFilter::NEAREST,
        backend::SamplerAddressMode::REPEAT, backend::SamplerAddressMode::REPEAT
    });
    bg->setPosition(visibleSize.width / 2, visibleSize.height / 2);

    return bg;
}

cocos2d::Node* MainMenu::createMenu()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();

    TTFConfig labelConfig("fonts/arial.ttf", 72);
    labelConfig.outlineSize = 3;

    auto menu = Menu::createWithArray(
    {
        MenuItemLabel::create(Label::createWithTTF(labelConfig, "Play", TextHAlignment::CENTER),
                              CC_CALLBACK_0(MainMenu::openLevelMenu, this)),
        MenuItemLabel::create(Label::createWithTTF(labelConfig, "Exit", TextHAlignment::CENTER),
                              CC_CALLBACK_0(Director::end, Director::getInstance()))
    });
    menu->alignItemsVerticallyWithPadding(50);

    auto bg = Sprite::create();
    bg->setTextureRect(Rect(Vec2::ZERO, menu->getContentSize() / 2));
    bg->setColor(Color3B::BLACK);
    bg->setOpacity(255 * 0.75);
    bg->setPosition(visibleSize.width / 2, visibleSize.height / 2);
    bg->addChild(menu);
    menu->setPosition(bg->getContentSize().width / 2, bg->getContentSize().height / 2);
    bg->setName("MainMenu");

    return bg;
}

Node* MainMenu::createLevelMenu()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    TTFConfig labelConfig("fonts/arial.ttf", 72);
    labelConfig.outlineSize = 3;

    Vector<MenuItem*> items;

    GridSize sizes[] =
    {
        GridSize::QUATTUOR, GridSize::SEX, GridSize::OCTO
    };

    for(auto i = 0; i < 3; ++i)
    {
        auto bg = Sprite::create();
        bg->setTextureRect(Rect(Vec2::ZERO, visibleSize * 0.2f));
        bg->setColor(Color3B::BLACK);
        bg->setOpacity(255 * 0.75f);

        auto size = std::to_string(static_cast<int>(sizes[i]));
        auto label = Label::createWithTTF(labelConfig, size + "x" + size, TextHAlignment::CENTER);
        label->setPosition(bg->getContentSize() / 2);
        bg->addChild(label);
        items.pushBack(MenuItemSprite::create(bg, nullptr, CC_CALLBACK_0(MainMenu::startGame, this, sizes[i])));
    }

    auto menu = Menu::createWithArray(items);
    menu->setPosition(visibleSize / 2);
    menu->alignItemsHorizontallyWithPadding(50);

    menu->setName("LevelMenu");

    return menu;
}

void MainMenu::openLevelMenu()
{
    this->getChildByName("MainMenu")->setVisible(false);
    this->getChildByName("LevelMenu")->setVisible(true);
}

void MainMenu::startGame(const GridSize &size)
{
    Director::getInstance()->replaceScene(GameplayScene::createScene(size));
}
