/************************************************************************
    match3
    Copyright (C) 2020  Amiyo Hisamov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "SplashScreen.h"

#include "MainMenu.h"
#include "iostream"

USING_NS_CC;

Scene* SplashScreen::createScene()
{
    return SplashScreen::create();
}

bool SplashScreen::init()
{
    if (!Scene::init())
    {
        return false;
    }
    this->addChild(createAuthorLabel());

    this->scheduleOnce(CC_SCHEDULE_SELECTOR(SplashScreen::toMainMenu), 2.0f);

    return true;
}

cocos2d::Menu* SplashScreen::createAuthorLabel()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();

    auto menu = Menu::createWithArray(
    {
        MenuItemLabel::create(Label::createWithTTF("Created by", "fonts/arial.ttf",
                              24, Size::ZERO, TextHAlignment::CENTER)),
        MenuItemLabel::create(Label::createWithTTF("amiyo", "fonts/PressStart2P-Regular.ttf",
                              42, Size::ZERO, TextHAlignment::CENTER))
    });
    menu->setPosition(visibleSize.width / 2, visibleSize.height / 2);
    menu->alignItemsVertically();
    menu->setEnabled(false);

    return menu;
}

void SplashScreen::toMainMenu(float dt)
{
    Director::getInstance()->replaceScene(TransitionFade::create(1.0, MainMenu::createScene()));
}
