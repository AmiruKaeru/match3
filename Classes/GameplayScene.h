/************************************************************************
    match3
    Copyright (C) 2020  Amiyo Hisamov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#pragma once

#include "cocos2d.h"

#include <map>
#include <set>
#include <utility>

enum class GridSize
{
    NONE = 0,
    QUATTUOR = 4,
    SEX = 6,
    OCTO = 8,
};

class GameplayScene : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene(const GridSize &size);

    virtual bool init();

    void onMouseDown(cocos2d::Event *event);

    CREATE_FUNC(GameplayScene);

private:

    enum class ItemType
    {
        NONE = 0,
        APPLE,
        BANANAS,
        CHERRIES,
        KIWI,
        MELON,
        ORANGE,
        PINEAPPLE,
        STRAWBERRY,

        MIN = APPLE,
        MAX = STRAWBERRY,
    };

    void restart();
    void mainMenu();

    // Creators
    cocos2d::Sprite* createBackground();
    cocos2d::Label* createScore();
    void createGrid();
    cocos2d::Sprite* createItem(const ItemType &type = static_cast<ItemType>(cocos2d::random(static_cast<int>(ItemType::MIN), static_cast<int>(ItemType::MAX))));
    void createItemAnimation();
    cocos2d::Node* createExitButton();
    cocos2d::Node* createGameOverMenu();

    // Modifiers
    void addScore(int score);
    void swapItems(cocos2d::Node *a, cocos2d::Node *b);
    void moveItems();

    // Helpers
    std::string Vec2ToString(const cocos2d::Vec2 &vec);
    cocos2d::Vec2 StringToVec2(const std::string &string);
    std::string ItemTypeToString(const ItemType &type);
    ItemType StringToItemType(const std::string &string);
    ItemType Vec2ToItemType(const cocos2d::Vec2 &vec);
    bool matchItem(cocos2d::Node* node);
    void checkGrid();
    void lockInput(float time);
    void gameOver();
    bool checkMovements();
    bool checkNearItems(const cocos2d::Vec2 &pos, const ItemType &type);

    cocos2d::Label* _score;
    cocos2d::Texture2D* _gridBG;
    cocos2d::Node* _gridNode;

    std::map<ItemType, cocos2d::Texture2D*> _itemTexture;
    cocos2d::Animation* _itemAnim;
    cocos2d::Animation* _itemDestroyAnim;

    cocos2d::Node* _selectedItem;
    std::set<std::string> _neighborhood;
    std::set<cocos2d::Node*> _matches;
    bool _isBusy;
};
