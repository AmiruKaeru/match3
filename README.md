# Match3

Just simple match three game created with cocos2d.

## How to build

```bash
$ cd cocos2d
$ python download-deps.py
$ git submodule update --init
$ cd ..
$ mkdir build
$ cd build
$ cmake ..
$ cmake --build . --target Match3
```